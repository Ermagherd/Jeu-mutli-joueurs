'use strict'

var http = require('http');
var express = require('express');
var app = express();


app.get('/', function(req, res){
    res.sendFile(__dirname + '/i.html');
});

var server = http.createServer(app);
var io = require('socket.io')(server);

io.on('connection', function(socket){
    console.log('New connection');

    io.emit('for everyone', 'a new client has joined us !')
});


server.listen(5555, function (err){
    console.log('Port 5555 sur écoute')
})