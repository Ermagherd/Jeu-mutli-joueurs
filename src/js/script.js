'use strict'
///////////////////////////////////////////////////////////////
///////////////////////// INITALISATION ///////////////////////
///////////////////////////////////////////////////////////////
const canvas = document.getElementById("mycanvas");
const ctx = canvas.getContext('2d');

///////////////////////////////////////////////////////////////
/////////////////////////// VARIABLE //////////////////////////
///////////////////////////////////////////////////////////////


let myAnimation;
let point = true;
let win = true;
let joueur1Y;
let joueur2Y;
let joueurGauche = true;
let joueurDroit = false;

///////////////////////////////////////////////////////////////
///////////////////////// LES OBJETS //////////////////////////
///////////////////////////////////////////////////////////////
const ball = {
    x : canvas.width/2,
    y : canvas.height/2,
    radius : 10,
    velocityX : 5,
    velocityY : 5,
    speed : 11,
    color : "RED"
}

const joueur1 = {
    x : 0, // left side of canvas
    y : (canvas.height - 100)/2, // -100 the height of paddle
    width : 10,
    height : 100,
    score : 0,
    color : "GREEN"
}

const joueur2 = {
    x : canvas.width - 10, // - width of paddle
    y : (canvas.height - 100)/2, // -100 the height of paddle
    width : 10,
    height : 100,
    score : 0,
    color : "MAGENTA"
}

const filet = {
    x : (canvas.width - 2)/2,
    y : 0,
    height : 15,
    width : 2,
    color : "WHITE"
}

///////////////////////////////////////////////////////////////
///////////////////////// LES FONCTIONS ///////////////////////
///////////////////////////////////////////////////////////////
// Fonction pour dessiner soit le terrain soit les raquettes
function drawRect(x, y, w, h, color){
    ctx.fillStyle = color;
    ctx.fillRect(x, y, w, h);
}

function drawBalle(x, y, r, color){
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.arc(x,y,r,0,Math.PI*2,true);
    ctx.closePath();
    ctx.fill();
}

// Engagement de la balle après un point d'un des joueurs
function resetBall(){
    ball.x = canvas.width/2;
    ball.y = canvas.height/2;
    ball.velocityX = -ball.velocityX;
    ball.speed = 11;
}

function drawFilet(){
    for(let i = 0; i <= canvas.height; i+=30){
        drawRect(filet.x, filet.y + i, filet.width, filet.height, filet.color);
    }
}

function drawScore(text,x,y){
    ctx.fillStyle = "#FFF";
    ctx.font = "75px fantasy";
    ctx.fillText(text, x, y);
}
///////////////////////////////////////////////////////////////
//////////////////////// KEY CODE  ////////////////////////////
///////////////////////////////////////////////////////////////

//  Paramètre à faire retirer par la suite
window.addEventListener('keydown',function(verrifClavier){
    switch(verrifClavier.keyCode){

        case 38:
            game();
            break;
        case 32: // Bas joueur1
            cancelAnimationFrame(myAnimation);
            break;
    }
})
///////////////////////////////////////////////////////////////
//////////////////////// MOUSE  ///////////////////////////////
///////////////////////////////////////////////////////////////
// Ecoute de la souris pour faire bouger la raquette
if(joueurGauche){
    canvas.addEventListener("mousemove", getMousePos);
    
    function getMousePos(evt){
        let rect = canvas.getBoundingClientRect();
        
        joueur1.y = evt.clientY - rect.top - joueur1.height/2;
        // console.log(joueur1.y)
        joueur1Y = joueur1.y;
    }
} else {
    canvas.addEventListener("mousemove", getMousePos);
    
    function getMousePos(evt){
        let rect = canvas.getBoundingClientRect();
        
        joueur2.y = evt.clientY - rect.top - joueur2.height/2;
        // console.log(joueur1.y)
        joueur2Y = joueur2.y;
    }
};

// Lance le jeu
window.addEventListener('click', function(evenement){
    game()
})


///////////////////////////////////////////////////////////////
/////////////////// FONCTION COLLISION  ///////////////////////
///////////////////////////////////////////////////////////////
function collision(b,p){
    p.top = p.y;
    p.bottom = p.y + p.height;
    p.left = p.x;
    p.right = p.x + p.width;
    
    b.top = b.y - b.radius;
    b.bottom = b.y + b.radius;
    b.left = b.x - b.radius;
    b.right = b.x + b.radius;
    
    return p.left < b.right && p.top < b.bottom && p.right > b.left && p.bottom > b.top;
}

function update(){
    
    // Condition du score
    if( ball.x - ball.radius < 0 ){
        joueur2.score++;
        resetBall();
    }else if( ball.x + ball.radius > canvas.width){
        joueur1.score++;
        resetBall();
    }
    
    // Condition du match
    if(joueur2.score > 4){
        console.log('joueur 2 gagne')
        cancelAnimationFrame(game);
        win =false;
    }
    if(joueur1.score > 4){
        console.log('joueur 2 gagne')
        cancelAnimationFrame(game);
        win = false
    }

    // Vélocité de la balle
    ball.x += ball.velocityX;
    ball.y += ball.velocityY;
    

    //Cacul joueur 2 (afin de voir la jouabilité)
    // if(!joueurDroit){
    //     joueur2.y += ((ball.y - (joueur2.y + joueur2.height/2)))*0.1;
    // } else{
    //     joueur1.y += ((ball.y - (joueur1.y + joueur1.height/2)))*0.1;

    // }
    
    // Quand la balle touche le haut ou le bas du terrain
    if(ball.y - ball.radius < 0 || ball.y + ball.radius > canvas.height){
        ball.velocityY = -ball.velocityY;
    }
    
    // Check si la balle touche la raquette du joueur1 ou 2 
    let joueur = (ball.x + ball.radius < canvas.width/2) ? joueur1 : joueur2;
    
    // Si la balle touche la raquette
    if(collision(ball,joueur)){
        // pointDeCollision définie le point de collision sur la raquette, afin de modifier l'angle de rebond
        let pointDeCollision = (ball.y - (joueur.y + joueur.height/2));
        // pointDeCollision est un nombre qui est compris entre -1 (bas de la raquette du joueur) et 1 (haut de la raquette du joueur)
        pointDeCollision = pointDeCollision / (joueur.height/2);
        

        //Orientation de la balle lors d'une collision (haut de la raquette => -45°/milieu de la raquette => 0°/bas de la raquette 45°)
        // Math.PI/4 => 45°
        let angleRad = (Math.PI/4) * pointDeCollision;
        
        //Changement de la direction de vélocité du X et Y
        let direction = (ball.x + ball.radius < canvas.width/2) ? 1 : -1;
        ball.velocityX = direction * ball.speed * Math.cos(angleRad);
        ball.velocityY = ball.speed * Math.sin(angleRad);
        
        // Vistesse de la balle lors d'un contact avec la raquette
        ball.speed += 0.1;
    }
}
let j1;
function dessin(){
    
    
    drawRect(0, 0, canvas.width, canvas.height, "#000");
    drawScore(joueur1.score,canvas.width/4,canvas.height/5);
    drawScore(joueur2.score,3*canvas.width/4,canvas.height/5);
    drawFilet();
    console.log('j1 : ', j1)
    drawRect(joueur1.x, j1, joueur1.height, joueur1.color);
    console.log('j1b : ', j1)
    drawRect(joueur2.x, joueur2.y, joueur2.width, joueur2.height, joueur2.color);
    drawBalle(ball.x, ball.y, ball.radius, ball.color);

}
function game(){
    update();
    dessin();
    if(win){

        myAnimation = window.requestAnimationFrame(game)
    }
}

// myAnimation = window.requestAnimationFrame(game)

