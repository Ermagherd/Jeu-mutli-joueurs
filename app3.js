'use strict'

const express = require('express');
const app = express();
const http = require('http').Server(app)
const io = require('socket.io')(http);
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:21017';
const dbName = 'back';
const session = require('express-session');

// const db = require('./db');

app.use('/css', express.static(__dirname + '/src/css'));
app.use('/js', express.static(__dirname + '/src/js'));

app.set('view engine', 'pug');
app.set('views', './page');


// Route ==> pug
app.get('/', function(req, res){
    res.render('accueil');
});
app.get('/index.html', function(req, res){
    res.render('accueil');
});
app.get('/inscription.html', function(req, res){
    res.render('inscription');
});
app.get('*', function(req, res){
    res.render('404');
});



app.post('/accueil', (req, res) => {
	db.getDB((dbClient, close) => {
		const collection = dbClient.collection('user');
		collection.find({ username: req.body.username }).toArray((err, docs) => {
			close();
			if (err) {
				return res.status(500).json({
					type: "Server error",
					msg: "Erreur du Serveur"
				})
			}
			if (docs.length === 0) {
				return res.status(500).json({
					type: "Form error",
					msg: "Nom ou mot de passe invalide"
				})
			}
			var user = docs[0];
			bcrypt.compare(req.body.password, user.password, function (err, result) {
				if (err) {
					return res.status(500).json({
						type: "Server error",
						msg: "Erreur du Serveur"
					})
				}
				if (result) {
					req.session.user = {
						username: user.username,
						_id: user._id
					}
					return res.json({
						msg: "User connected !"
					})
				} else {
					return res.status(500).json({
						type: "Form error",
						msg: "Nom ou mot de passe invalide"
					})
				}
			});
		});

	});

});

  io.on('connection', function(socket){
    console.log('New connection');
    console.log('id : ', socket.id)
    io.emit('for everyone', 'HipHipHip a new client has joined us !')

    socket.on('joueur1', function(coords){
        console.log(`joueur1 `, coords)
    });

    socket.on('message', function(data){
        console.log('data :', data)
    });



});

http.listen(4455, function(err){
  if(err){
    console.log('Problème connexion serveur');
} else{
    console.log('Port 4455 sur écoute');
}
});