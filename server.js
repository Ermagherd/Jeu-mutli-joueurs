'use strict';

const express = require('express');
const app = express();
const http = require('http').Server(app);
const fs =require('fs')


// Gestion des fichiers statiques
app.use('/css', express.static(__dirname + '/src/css'));
app.use('/js', express.static(__dirname + '/src/js'));
console.log(__dirname + '/js')


// // Fonction qui produit la réponse http
// var writeResponse = function writeHTTPResponse(httpReponse, responseCode, responseBody){
//     httpReponse.writeHead(responseCode, {
//         'Content-type': 'index.html; charset=UTF-8',
//         'Content-length': responseBody.length
//     });
//     httpReponse.write(responseBody, function(){
//         httpReponse.end();
//     });
// };

//Fonction qui renvoie la réponse 404 si le document n'est pas trouvé.
const error404 = function(httpReponse, httpReponseCode, text404){
    // writeResponse(httpReponse, 404,
         text404 =`<doctype html!><html><head><meta charset='UTF-8'><title>Erreur 404</title></head><body><h1>Erreur 404</h1><h3>The request URL could not be found</h3><h3>La page demandé n'existe pas</h3></body>`;

         httpReponse.writeHead(httpReponseCode, {
        'Content-Type': 'text/html; charset=UTF-8',
        'Content-Length': (typeof text404 ==='string') ? Buffer.byteLength(text404) : text404.length
    });
    httpReponse.write(text404, () =>{
        httpReponse.end();
    });
};

////////////////////////// CREATION SERVER //////////////////////////

app.use(function(request, httpReponse){
    if(request.url === '/'){
        const filename = `index.html`;
        fs.readFile(filename, function(error, fileData){
            if(error){
                error404(httpReponse);
            } else {
                writeResponse(httpReponse, 200, fileData);
            }
        })

    } else {
        if(request.url === '/inscription.html'){
            fs.readFile('inscription.html', function(error, fileData){
                if(error){
                    error404(httpReponse);
                } else {
                    writeResponse(httpReponse, 200, fileData);
                }
            })
        } else {
            if(request.url === '/index.html'){
                fs.readFile('index.html', function(error, fileData){
                    if(error){
                        error404(httpReponse);
                    } else {
                        writeResponse(httpReponse, 200, fileData);
                    }
                })
            } else {
                error404(httpReponse);
            }
        } 
    }
});


///////////////////////////// WEBSOCKET ///////////////////////////


// const webSocket = require(`websocket`);

// const webSocketServer = new WebSocketServer({
//     httpServer : httpServer
// });

const io = require(`socket.io`)(http);

// const io = socketIo(httpServer);

io.on(`connection`, function(socket){
    console.log(socket.id)
    console.log(`Un client s'est connecté`);

    socket.on(`unEvenement`, function(message){
        console.log(`un évènement est reçu`);

        io.emit(`unAutreMessage`, {
            
        })
    })
})



//////////////////////////// ECOUTE DU SERVER //////////////////////////// 

app.listen(8888, function(error){
    if(error){
        console.log(`Impossible d'associer le serveur HTTP au port 8888`)
    } else {
        console.log(`Port 8888 sur écoute`)
    }
})