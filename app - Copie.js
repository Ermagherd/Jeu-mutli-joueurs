'use strict'

const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:21017/back';
const dbName = 'back';
const session = require('express-session');
const db = require('./db');

app.use('/css', express.static(__dirname + '/src/css'));
app.use('/js', express.static(__dirname + '/src/js'));

app.set('view engine', 'pug');
app.set('views', './page');


// Route ==> pug
app.get('/', function(req, res){
    res.render('accueil');
});
app.get('/index.html', function(req, res){
    res.render('accueil');
});
app.get('/inscription.html', function(req, res){
    res.render('inscription');
});
app.get('*', function(req, res){
    res.render('404');
});

// app.use(session({
//     secret: `1234566789SECRET`,
//     saveUninitialized: false,
//     resave: false
// }))


db.connect('mongodb://localhost:27017/back', function(err) {
  if (err) {
    console.log('Impossible de se connecter à la base de données.');
    process.exit(1);
  } else {
    // app.listen(8080, function() {
      console.log('Le serveur est disponible sur le port 8080');
    // });
  }
});

// app.post('/accueil', function(req, res, next){
//     MongoClient.connect(URL, {userNewUrlParser: true}, function(err, client){
//         if(err){
//             console.log('erreur connection DB')
//         } else {

//             console.log('connexion db')
//             const db = client.db(dbName);
//             const collection = db.collection('user');
            
//         collection. find({}).toArray(function(err, docs){
//             client.clode()
//             let idDB = false;
//             let passDB = false;
//             let pseudoform = req.body.id;
//             let passform = req.body.password;
            
//             for (i=0; i < docs.length; i++){
//                 if(docs[i].pseudo === pseudoform){
//                     idDB = true;
//                 } if (docs[i].password === passform){
//                     passDB = true;
//                     req.session.user = docs[i]._id;
//                     id = docs[i].pseudo;
//                 }
//             }
//             if(idDB && passDB){
//                 res.render('accueil', {Mesage: 'Welcome body'});
//                 req.session._id = idform;
//         } else {
//             res.render(`accueil`, {Message: `Sorry body`})
//         }
        
//     })
    
// }
//     })

// })
// Création du serveur
// const server = http.createServer(app);

// Connection avec le serveur
io.on('connection', function(socket){
    console.log('New connection');
    console.log('id : ', socket.id)
    io.emit('for everyone', 'HipHipHip a new client has joined us !')

    socket.on('joueur1', function(coords){
        console.log(`joueur1 `, coords)
    });

    socket.on('message', function(data){
        console.log('data :', data)
    });



});




// Ecoute du serveur
http.listen(7777, function (err){
    if(err){
        console.log('Problème connexion serveur');
    } else{
        console.log('Port 7777 sur écoute');
    }
})