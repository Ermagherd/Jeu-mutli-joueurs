'use strict'

const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
const url = 'mongodb://localhost:27017';
const dbName = 'back';

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }))

app.use('/css', express.static(__dirname + '/src/css'));
app.use('/js', express.static(__dirname + '/src/js'));

app.set('view engine', 'pug');
app.set('views', './page');

app.get('/',function(req,res,next) {
    res.render('accueil',{title:'Accueil du Blog'});
});

app.get('/accueil.html',function(req,res,next) {
    res.render('accueil',{title:'Accueil de l\'administration'});
});

app.get('/connexion.html', function(req, res, next){
    res.render('connexion')
})
app.get('/connexion2.html', function(req, res, next){
    res.render('connexion2')
})

app.get('/inscription.html', function(req, res, next){
    res.render('formu', {title:'Inscription', sstitle: 'Veuillez renseigner les différents champs pour vous inscrire'})
})

app.post('/approuver',function(req,res,next) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, client) {
        if(err){
            console.log('connexion err ', err)
        } else {
            console.log('connexion de connexion')
            const collection = client.db('back').collection('user');
            console.log('passé par ici')
            collection.find({}).toArray(function (err, result) {
                console.log(result[0])
                let pseudo = false;
                let password = false;
                let pseudoFrom = req.body.pseudo;
                let PasswordFrom = req.body.password;

                for( let i = 0; i < result.length; i++){
                    console.log('for')
                    if(result[i].pseudo === pseudoFrom){
                        pseudo = true;
                    } if (result[i].password === PasswordFrom){
                        password = true                    
                }
            }
            if(pseudo && password){
                res.render('accueil', {message: 'Vous êtes connecté'})
            } else {
                res.render('connexion', {title: 'Mauvaise Identification', message: 'Le mot de passe est invalide avec ce pseudo'})
            }
            })
    }
    });
});
app.post('/approuver2',function(req,res,next) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, client) {
        if(err){
            console.log('connexion err ', err)
        } else {
            console.log('connexion de connexion')
            const collection = client.db('back').collection('user');
            console.log('passé par ici')
            collection.find({}).toArray(function (err, result) {
                console.log(result[0])
                let pseudo = false;
                let password = false;
                let pseudoFrom = req.body.pseudo;
                let PasswordFrom = req.body.password;

                for( let i = 0; i < result.length; i++){
                    console.log('for')
                    if(result[i].pseudo === pseudoFrom){
                        pseudo = true;
                    } if (result[i].password === PasswordFrom){
                        password = true                    
                }
            }
            if(pseudo && password){
                res.render('accueil', {message: 'Vous êtes connecté'})
            } else {
                res.render('connexion2', {title: 'Mauvaise Identification', message: 'Le mot de passe est invalide avec ce pseudo'})
            }
            })
    }
    });
});


app.get('/add-user',function(req,res,next) {
    res.render('formu',{title:'Inscription'});
});


app.post('/envoie', function(req,res,next) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, client) {
        const db = client.db(dbName);
        const collection = db.collection('user');
        collection.insertOne({
            pseudo:req.body.pseudo,
            password:req.body.password,
        },function(err,result) {
            if (err) {
                console.log('erreur')
                res.status(503);
                next();
            }
            client.close();
            res.render('formu',{title: 'Inscription', message:'Utilisateur ' + req.body.pseudo + ' ajouté, veuillez retournez à la page d\'accueil'});
        });
    });
});


app.use(function(req,res,next) {
    if (res.statusCode == 503) {
        res.send('Erreur 503');
    } else {
        res.render('404',{title:'Page indisponible'});
    }
});
// Création du serveur
// const server = http.createServer(app);
// Connection avec le serveur
let joueur1Y;
let joueur2Y;
let racket = {};
io.on('connection', function(socket){
    console.log('New connection');
    console.log('id : ', socket.id)
    io.emit('for everyone', 'HipHipHip a new client has joined us !')

    
    socket.on('joueur', function(coords){
        let raquetteArray = Object.keys(racket).map(key => racket[key]);
        raquetteArray.push(coords)
        joueur1Y = coords;
        console.log('test ', joueur1Y)
        io.emit('coords', raquetteArray)
        // socket.broadcast.emit('newSquare', clientObject);
        console.log('envoyé')

        // const myRacket = new Racket();
        // racket[socket.id] = myRacket;
        // const clientObject = myRacket.getClientObject();

        // io.emit('myRacketCreator', clientObject);
        // io.broadcast.emit('', clientObject)
        
    });
    
    socket.on('joueur2', function(coords){
        console.log(`joueur1 `, coords)
        joueur2Y = coords;
        console.log('test ', joueur2Y)
        io.emit('coords', joueur2Y)
        console.log('envoyé')
        
    });


});



// Ecoute du serveur
http.listen(8888, function (err){
    if(err){
        console.log('Problème connexion serveur');
    } else{
        console.log('Port 8888 sur écoute');
    }
})